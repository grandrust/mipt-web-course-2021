# Work with NPM

## Add module

```
npm install --save-dev jest
```
installs jest as `devDependencies` (see package.json).

All installed modules will be placed into `.node_modules` folder. The folder is on the same location like `package.json` file.

## Update script section in `package.json`

```
...

 "scripts": {
    "start": "node index.js",
    "test": "jest"
  },

...
```

## Run NPM scripts

Run tests

```
npm run test
```

Run app
```
npm run start
```
