
class Todo implements ITodo
{
    constructor(private action: string)
    {
    }

    public do(): void {
        console.log(`I do: '${this.action}'`);
    }
}

export default Todo